import java.util.Arrays;

public class HW4 {
    //Задан массив случайных чисел. Написать метод который вернет массив в котором сначала будут
    // все четные элементы, а потом все нечетные из исходного

    public static void main(String[] args) {
        int [] arrToExecute = {1,3,2,4};
        HW4 hw4 = new HW4();
        hw4.changeOrderAccording(arrToExecute);
        Arrays.asList(arrToExecute).stream().forEach(l-> System.out.println());



    }



    public void changeOrderAccording(int[] arr){
        int oddFlag = 0, evenFlag = 0;
        for (int i =oddFlag; i<arr.length; i++) {
            if ((arr[i] % 2) != 0) {
                oddFlag = i+1;
            for (int j=evenFlag; j<arr.length; j++){
                if ((arr[j]%2)==2){
                    evenFlag = j+1;

                    swap(arr[i],arr[j]);
                    break;
                }
            }
            }

        }
    }

    public void swap (int x,int y){
        int temp = x;
        x = y;
        y = temp;
    }
}
